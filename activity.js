
//USERS
db.users.insertMany([

{

   "firstName": "Naruto",

   "lastName": "Uzumaki",

   "email": "gotNerfedWhenBorutoCame@gmail.com",

   "password":"rasengan",

   "isAdmin":false

},

{

   "firstName": "Sasuke",

   "lastName": "Uchiha",

   "email": "iLoveSakura@gmail.com",

   "password":"iLiedInMyEmail",

   "isAdmin":false

},

{

   "firstName": "Luffy",

   "lastName": "Monkey",

   "email": "iWillBeThePirateKing.com",

   "password":"meatmeat",

   "isAdmin":false

},

{

   "firstName": "Zoro",

   "lastName": "Roronoa",

   "email": "greatAtDirection@gmail.com",

   "password":"sakesake",

   "isAdmin":false

},

{

   "firstName": "Sanji",

   "lastName": "Vinsmoke",

   "email": "soHandsome@gmail.com",

   "password":"iAmNotGay",

   "isAdmin":false

}

])

//COURSES
db.courses.insertMany([
{
    "name":"Math 101",
    "description":"teaches Math",
    "price":1000,
    "isActive":true
},
{
    "name":"perseverance 101",
    "description":"teaches how not to give up",
    "price":2000,
    "isActive":true
},
{
    "name":"Maps 101",
    "description":"teaches directions",
    "price":3000,
    "isActive":true
}
])

//UPDATE
db.users.updateOne({"firstName": "Luffy"},{$set:{"isAdmin":true}})

//READ
db.users.find({"isAdmin": false})

//UPDATE
db.courses.updateOne({"name": "Math 101"},{$set:{"isAdmin":false}})